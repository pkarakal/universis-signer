# universis-signer

Universis pdf signing tools

![Universis Signer](./universis-signer-screenshot.png)

universis-signer is a tiny client-side application which enables pdf signing by using 
either a PKCS12 local keystore or a PKCS11 keystore of a USB token. These signing operations are part of 
[@universis/reports](https://gitlab.com/universis/reports.git) package and allow users to sign student certificates,
transcripts and any other document that requires a digital signature.

## Installation

__Important Note: Universis Signer runs as service in all operating systems (Linux, Windows or macOS) and installation needs administrative privileges to complete__

### 32-bit Linux

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-linux-x86.zip

- Extract compressed package:

      unzip universis-signer-linux-x86.zip
      mv universis-signer-linux-x86 /opt/universis-signer-linux-x86
      sudo chown -R root:root /opt/universis-signer-linux-x86 

- Install service:

      cd /opt/universis-signer-linux-x86
      sudo ./service.sh

### 64-bit Linux

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-linux-x64.zip

- Extract compressed package:

      unzip universis-signer-linux-x64.zip
      mv universis-signer-linux-x64 /opt/universis-signer-linux-x64
      sudo chown -R root:root /opt/universis-signer-linux-x64 

- Install service:

      cd /opt/universis-signer-linux-x64
      sudo ./service.sh

### 32-bit Windows

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-windows-x86.zip

- Unzip compressed package

- Move `universis-signer-windows-x86` directory to system root e.g.  `C:\`

- Execute service.bat

### 64-bit Windows

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-windows-x64.zip

- Unzip compressed package

- Move `universis-signer-windows-x64` directory to system root e.g.  `C:\`

- Execute service.bat

### macOS

- Download installation media from:
https://api.universis.io/releases/universis-signer/latest/universis-signer-macosx-x64.zip

- Extract compressed package:

      unzip universis-signer-macosx-x64.zip
      mv universis-signer /opt/universis-signer

- Install service:

      cd /opt/universis-signer
      sudo ./launcher.sh

## Development usage

### Start universis-signer with a local PKCS12 key store:

        java -jar universis-signer-1.0.2.jar -keyStore ./keystore.p12 -storeType PKCS12
    
where `keyStore` arguments holds the path of the local PKCS12 key store.
    
### Start universis-signer with a PKCS11 key store of a USB token:

        java -jar universis-signer-1.0.2.jar -keyStore none -storeType PKCS11 -providerArg ./eToken.cfg
    
where `storeType` argument holds key store type (PKCS11) and `providerArg` contains the path of USB token configuration e.g.

    name=eToken
    library=/usr/local/lib/libeTPkcs11.dylib
    slot=0

or any other USB token required configuration

## List key store certificates

The following request

    curl --location --request GET 'localhost:2465/keystore/certs' \
    --header 'Authorization: Basic dXNlcjpzZWNyZXQ='

returns a list of the available certificates e.g.

    [
        {
            "version": 3,
            "subjectDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
            "sigAlgName": "SHA256withRSA",
            "sigAlgOID": "1.2.840.113549.1.1.11",
            "issuerDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
            "serialNumber": 11651655752893169577,
            "notAfter": "Jun 18, 2022 9:29:12 AM",
            "notBefore": "Jun 18, 2020 9:29:12 AM",
            "expired": false,
            "thumbprint": "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
            "commonName": "Universis Test Signer"
        }
    ]
    
Important note: In basic HTTP authentication, a request contains a header field 
in the form of Authorization: Basic <credentials>, where credentials is the Base64 encoding of ID and password joined by a single colon :

`user` parameter is optional (use a default value like "user") and `password` parameter is the password of the key store.
 

## Sign pdf document

![Signed PDF Document](./screenshot-signed-pdf.png)

The following request passes an input file and returns a signed pdf e.g.

    curl --location --request POST 'localhost:2465/sign/' \
    --header 'Authorization: Basic dXNlcjoxMjM0' \
    --form 'thumbprint=f641d66d49ba43bf911e873889d7fa0e32e26ce6' \
    --form 'position=20,10,320,120' \
    --form 'file=@/home/user/Downloads/document.pdf'

where `thumbprint` parameter is the thumbprint of the certificate that is going to used to sign pdf document,
`position` is an optional parameter of the position of the signature block and `file` contains the pdf document to be signed.

Important note: In basic HTTP authentication, a request contains a header field 
in the form of Authorization: Basic <credentials>, where credentials is the Base64 encoding of ID and password joined by a single colon :

`user` parameter is optional (use a default value like "user") and `password` parameter is the password of the key store.
 
 ## Build from sources
 
Clone universis-signer repository:
 
    git clone https://gitlab.com/universis/universis-signer.git
    
Run maven and build package:

    mvn package

## Create PKCS12 store

Execute the following command to create a self-signed certificate and add it to a PKCS12 keystore

    openssl req -newkey rsa:2048 -x509 -keyout key.pem -out cert.pem -days 365
    cat key.pem cert.pem > cert-bundled.pem

    openssl pkcs12 -export -in cert-bundled.pem -out keystore.p12

## Use PKCS12 store

### Linux

- Stop universis-signer.service

      sudo systemctl stop universis-signer.service

- Copy PKCS12 store to universis-signer installation directory e.g /opt/universis-signer-linux-x64 or /opt/universis-signer-linux-x86

- Modify service configuration to use PKCS12 store

      sudo nano /etc/systemd/system/universis-signer.service

      [Unit]
      Description=Universis Signer

      [Service]
      Type=simple
      ExecStart=/bin/bash -c '"/opt/universis-signer-linux-x64/jre/bin/java" -cp "/opt/universis-signer-linux-x64/universis-signer.jar" org.universis.signer.SignerService -storeType PKCS12 -keyStore "/opt/universis-signer-linux-x64/keystore.p12"'
      User=root
      Group=root
      WorkingDirectory=/opt/universis-signer-linux-x64

      [Install]
      WantedBy=multi-user.target

- And finally start service

      sudo systemctl start universis-signer.service

### Windows

- Copy keystore.p12 in installation directory e.g. C:\universis-signer-windows-x64\

- Open windows registry editor `regedit.exe`, go to `HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Apache Software Foundation\Procrun 2.0\UniversisSigner\Parameters\Start` key and modify `Params` value in order to include PKCS12 store.

       -storeType
       PKCS12
       -keyStore
       C:\universis-signer-windows-x64\keystore.p12

- Restart Universis Signer service (Control Panel\Administrative Tools\Services)

![Universis Signer Regedit](./windows-regedit-screenshot.png)


### macOS

- Stop org.univeris.signer agent

      sudo launchctl unload /Library/LaunchAgents/org.universis.signer.plist

- Copy PKCS12 store to universis-signer installation directory e.g /opt/universis-signer/

- Modify service configuration to use PKCS12 store

      sudo nano /Library/LaunchAgents/org.universis.signer.plist

      <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
      <plist version="1.0">
      <dict>
        <key>KeepAlive</key>
        <false/>
        <key>Label</key>
        <string>org.universis.signer</string>
        <key>OnDemand</key>
        <true/>
        <key>ProgramArguments</key>
        <array>
                <string>/opt/universis-signer/jre/bin/java</string>
                <string>-jar</string>
                <string>/opt/universis-signer/universis-signer.jar</string>
                <string>-storeType</string>
                <string>PKCS12</string>
                <string>-keyStore</string>
                <string>/opt/universis-signer/keystore.p12</string>
        </array>
        <key>RunAtLoad</key>
        <true/>
        <key>StandardErrorPath</key>
        <string>/var/log/universis-signer.log</string>
        <key>StandardOutPath</key>
        <string>/var/log/universis-signer.log</string>
        <key>WorkingDirectory</key>
        <string>/opt/universis-signer</string>
      </dict>

- And finally load agent

      sudo launchctl load -w /Library/LaunchAgents/org.universis.signer.plist



