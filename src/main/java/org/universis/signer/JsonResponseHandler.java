package org.universis.signer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

import java.io.Serializable;
import java.util.Map;

/**
 * Formats responses to JSON
 */
public class JsonResponseHandler {
    private final Serializable any;
    public JsonResponseHandler(Serializable any) {
        this.any = any;
    }

    public String getText() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
        return mapper.writeValueAsString(this.any);
    }

    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> urlParams, NanoHTTPD.IHTTPSession session) throws JsonProcessingException {
        return NanoHTTPD.newFixedLengthResponse(this.getStatus(), this.getMimeType(), this.getText());
    }

    public String getMimeType() {
        return "application/json";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        if (this.any == null) {
            return NanoHTTPD.Response.Status.NO_CONTENT;
        }
        return NanoHTTPD.Response.Status.OK;
    }
}
