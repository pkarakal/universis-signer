package org.universis.signer;

public class SingerExtraOptions {
    public String fontFamily = null;
    public int fontSize = 10;
    public String reason = null;
    public String location = null;
    public String timestampServer = null;
}
