package org.universis.signer;

import sun.security.pkcs11.SunPKCS11;
import sun.security.pkcs11.wrapper.CK_SLOT_INFO;
import sun.security.pkcs11.wrapper.PKCS11;
import sun.security.pkcs11.wrapper.PKCS11Constants;
import sun.security.pkcs11.wrapper.PKCS11Exception;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Map;

/**
 *  org.universis.signer.Signer application configuration options
 */
public class SignerAppConfiguration {
    /**
     * A string which represents the type of keystore pkcs11 or pkcs12
     */
    public String storeType = "pkcs12";
    /**
     * A string which contains the path of token configuration
     */
    public String providerArg;
    /**
     * A string which contains the path of a pkcs12 key store
     */
    public String keyStore;
    /**
     * A string which contains the keystore password
     */
    public String keyStorePass;

    public boolean daemon;

    public KeyStore getKeyStore(String password) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore ks;
        // get pkcs12 store
        if (this.storeType.toLowerCase().equals("pkcs12")) {
            ks = KeyStore.getInstance(this.storeType);
            ks.load(new FileInputStream(this.keyStore), password.toCharArray());
            return ks;
        }
        // get pkcs11 store
        if (this.storeType.toLowerCase().equals("pkcs11")) {
            // add provider if missing
            Provider p = new sun.security.pkcs11.SunPKCS11(this.providerArg);
            if (Security.getProvider(p.getName()) == null) {
                Security.addProvider(p);
            }
            ks = KeyStore.getInstance(this.storeType);
            ks.load(null, password.toCharArray());
            return ks;
        }
        // not implemented
        throw new InvalidParameterException("The specified key store type has not implemented yet.");
    }

    public void tryCloseKeyStore(KeyStore ks) throws Exception {
        Provider provider = ks.getProvider();
        if (provider == null) {
            return;
        }
        if (this.storeType.toLowerCase().equals("pkcs11")) {
            this.closeKeyStore(ks);
        }
    }

    private void finalizeProvider(PKCS11 pkcs11) throws IOException {
        try {
            Field f = PKCS11.class.getDeclaredField("moduleMap");
            f.setAccessible(true);
            Map moduleMap = (Map) f.get(pkcs11);
            moduleMap.clear();
            pkcs11.C_Finalize(PKCS11Constants.NULL_PTR);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException | PKCS11Exception e) {
            throw new IOException("PKCS11 close failed", e);
        }
    }

    private void closeKeyStore(KeyStore ks) throws Exception {
        Provider provider = ks.getProvider();
        if(provider==null) {
            return;
        }
        try {
            Field f = SunPKCS11.class.getDeclaredField("p11");
            f.setAccessible(true);
            PKCS11 objectPKCS11 = (PKCS11)f.get(provider);
            finalizeProvider(objectPKCS11);
        } catch (Exception e) {
            e.printStackTrace();
        }
        provider.clear();
        ((SunPKCS11)provider).setCallbackHandler(null);
        Security.removeProvider(provider.getName());
        provider = null;
        ks = null;
        System.gc();
    }

    public SlotInfo[] getSlots(boolean availableOnly) throws PKCS11Exception, NoSuchFieldException, IllegalAccessException {
        if (this.storeType.toLowerCase().equals("pkcs12")) {
            return new SlotInfo[0];
        }
        // get pkcs11 store
        if (this.storeType.toLowerCase().equals("pkcs11")) {
            // add provider if missing
            Provider p = new sun.security.pkcs11.SunPKCS11(this.providerArg);
            Security.addProvider(p);
            Field f = SunPKCS11.class.getDeclaredField("p11");
            f.setAccessible(true);
            PKCS11 objectPKCS11 = (PKCS11)f.get(p);
            long[] slots = objectPKCS11.C_GetSlotList(availableOnly);
            SlotInfo[] res = new SlotInfo[slots.length];
            for (int i = 0; i < slots.length; i++) {
                long slotNumber = slots[i];
                CK_SLOT_INFO info = objectPKCS11.C_GetSlotInfo(slotNumber);
                res[i] = new SlotInfo() {{
                    slot = slotNumber;
                    slotDescription = new String(info.slotDescription).trim();
                    manufacturerID =  new String(info.manufacturerID).trim();
                    flags = info.flags;
                    hardwareVersion = info.hardwareVersion.toString();
                    firmwareVersion = info.firmwareVersion.toString();
                }};
            }
            return res;
        }
        return new SlotInfo[0];
    }

}
