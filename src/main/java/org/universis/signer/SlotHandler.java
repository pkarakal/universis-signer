package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import sun.security.pkcs11.wrapper.CK_SLOT_INFO;
import sun.security.pkcs11.wrapper.PKCS11Exception;

import java.util.List;
import java.util.Map;

public class SlotHandler  implements RouterNanoHTTPD.UriResponder {
    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        try {
            SignerAppConfiguration configuration = uriResource.initParameter(SignerAppConfiguration.class);
            if (configuration == null) {
                return new ServerErrorHandler("Application configuration cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            if (configuration.keyStore == null) {
                return new ServerErrorHandler("Invalid application configuration. Keystore cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            try {
                SlotInfo[] slots;
                boolean availableOnly = true;
                Map<String, List<String>> params = ihttpSession.getParameters();
                if (params.containsKey("availableOnly")) {
                    boolean notAvailableOnly = params.get("availableOnly").get(0).toLowerCase().equals("false");
                    if (notAvailableOnly) {
                        availableOnly = false;
                    }
                }
                slots = configuration.getSlots(availableOnly);
                // and finally return a JSON array
                NanoHTTPD.Response res = new JsonResponseHandler(slots).get(uriResource, map, ihttpSession);
                CorsHandler.enable(res);
                // set client origin if any
                Map<String, String> headers = ihttpSession.getHeaders();
                if (headers.containsKey("origin")) {
                    res.addHeader("Access-Control-Allow-Origin", headers.get("origin"));
                }
                return res;
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
                return new ForbiddenHandler().get(uriResource, map, ihttpSession);
            }
            catch (PKCS11Exception e) {
                e.printStackTrace();
                return new ServerErrorHandler(e.getMessage()).get(uriResource, map, ihttpSession);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
