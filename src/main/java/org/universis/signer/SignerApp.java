package org.universis.signer;

import com.sun.org.apache.xpath.internal.operations.Bool;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;

/**
 * Universis signer application
 */
public class SignerApp extends RouterNanoHTTPD {

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 2465;

    protected SignerAppConfiguration configuration;

    public SignerApp() throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.addMappings();
    }

    public SignerApp(SignerAppConfiguration configuration) throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.configuration = configuration;
        this.addMappings();
    }

    @Override
    public void start() throws IOException {
        this.start(NanoHTTPD.SOCKET_READ_TIMEOUT, this.configuration.daemon);
    }

    @Override
    public void addMappings() {
        super.addMappings();
        File publicDirectory = new File(SignerApp.class.getResource("public").getPath());
        addRoute("/", RootHandler.class);
        addRoute("/index.html", RootHandler.class);
        addRoute("/keystore/certs", KeyStoreHandler.class, this.configuration);
        addRoute("/sign/", SignerHandler.class, this.configuration);
        addRoute("/slots/", SlotHandler.class, this.configuration);

    }

}
