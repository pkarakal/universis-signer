package org.universis.signer;

public class SlotInfo {
    public long slot;
    public String slotDescription;
    public String manufacturerID;
    public long flags;
    public String hardwareVersion;
    public String firmwareVersion;
}
