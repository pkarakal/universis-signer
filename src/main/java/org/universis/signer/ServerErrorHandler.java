package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;

/**
 * Handles 500 Internal Server Error
 */
public class ServerErrorHandler extends ErrorHandler {

    public ServerErrorHandler() {
        super(NanoHTTPD.Response.Status.INTERNAL_ERROR);
    }
    public ServerErrorHandler(Exception e) {
        super(e);
    }
    public ServerErrorHandler(String message) {
        super(message);
    }
}
