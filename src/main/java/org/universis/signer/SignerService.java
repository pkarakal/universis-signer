package org.universis.signer;

import org.apache.commons.cli.*;

import java.io.IOException;

public class SignerService {

    protected SignerApp signerApp;

    public void init(String[] args) {
        try {
            Options options = new Options();
            Option keyStoreOption = new Option("ks", "keyStore", true, "keyStore file path e.g. ./keystore.p12 or none");
            keyStoreOption.setRequired(true);
            options.addOption(keyStoreOption);

            Option storeTypeOption = new Option("st", "storeType", true, "keyStore type e.g. PKCS11 PKCS12");
            storeTypeOption.setRequired(true);
            options.addOption(storeTypeOption);

            Option providerArgOption = new Option("pa", "providerArg", true, "keyStore provider argument e.g ./token.cfg for a PKCS11 key store");
            providerArgOption.setRequired(false);
            options.addOption(providerArgOption);

            Option daemonOption = new Option("d", "daemon", true, "start the thread daemon or not.");
            providerArgOption.setRequired(false);
            options.addOption(daemonOption);

            CommandLineParser parser = new DefaultParser();
            HelpFormatter formatter = new HelpFormatter();
            CommandLine cmd;

            try {
                cmd = parser.parse(options, args);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
                formatter.printHelp("java -jar universis-signer-x.x.x.jar", options);
                System.exit(1);
                return;
            }
            SignerAppConfiguration configuration = new SignerAppConfiguration();
            configuration.keyStore = cmd.getOptionValue("keyStore");
            configuration.storeType = cmd.getOptionValue("storeType");
            configuration.providerArg = cmd.getOptionValue("providerArg");
            configuration.daemon = Boolean.parseBoolean(cmd.getOptionValue("daemon"));
            // set configuration
            this.signerApp = new SignerApp(configuration);

        } catch (Exception e) {
            System.err.println("Couldn't init universis signer app:\n" + e);
        }
    }

    void start() throws IOException {
        if (this.signerApp != null) {
            this.signerApp.start();
        }
    }

    void stop() {
        if (this.signerApp != null) {
            this.signerApp.stop();
        }
    }

    void destroy() {
        this.signerApp = null;
    }


    public static void main(String[] args) {
        try {
            // create server
            SignerService service = new SignerService();
            service.init(args);
            // and start
            System.out.println("Universis signer app is starting.");
            service.start();
        } catch (Exception e) {
            System.err.println("Couldn't start universis signer app:\n" + e);
        }
    }


}
