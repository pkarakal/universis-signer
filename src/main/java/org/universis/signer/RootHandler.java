package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.io.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Handles / request
 */
public class RootHandler implements RouterNanoHTTPD.UriResponder {

    public String getText() throws IOException {
        return IOUtils.toString(RootHandler.class.getResourceAsStream("public/index.html"), StandardCharsets.UTF_8);
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.OK;
    }

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> urlParams, NanoHTTPD.IHTTPSession session) {
        try {
            NanoHTTPD.Response res = NanoHTTPD.newFixedLengthResponse(this.getStatus(), this.getMimeType(), this.getText());
            CorsHandler.enable(res);
            return res;
        } catch (IOException e) {
            return new ServerErrorHandler(e).get(uriResource, urlParams, session);
        }
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

}
